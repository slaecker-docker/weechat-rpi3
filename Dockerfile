FROM balenalib/raspberrypi3-alpine-python:edge-run
#FROM balenalib/amd64-alpine-python:edge-run

RUN [ "cross-build-start" ]

ENV UGID=9876 \
#    LANG="C.UTF-8" \
#    TERM="xterm-256color" \
    WEECHAT_HOME="/data/weechat" \
    WEECHAT_RELAY_PASSWORD="wcRelay9000"

# /!\ KEEP SPACE AT THE END
ARG PY_PKGS="py2-paho-mqtt "

RUN apk add --no-cache ${PY_PKGS}su-exec perl openssh-server mosh screen git weechat weechat-python weechat-perl && \
    apk add --no-cache gpm --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ --allow-untrusted && \
    git clone https://github.com/glowing-bear/glowing-bear /app

ADD run.sh /usr/local/bin/

WORKDIR /data

EXPOSE 2222 60000 8000 9000

VOLUME [ "/data" ]

HEALTHCHECK --start-period=60s CMD su-exec weechat screen -ls | egrep '^\s+[0-9]+\.weechat' > /dev/null 2>&1 || exit 1

ENTRYPOINT [ "/bin/sh", "/usr/local/bin/run.sh" ]

RUN [ "cross-build-end" ]
