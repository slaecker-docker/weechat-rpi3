#!/bin/sh

if [ -z "${WEECHAT_HOME}" ]; then
  echo "ERROR: Variable WEECHAT_HOME is empty!"
  exit 1
fi

# USER AND GROUP
addgroup -g ${UGID} weechat
adduser -h /data -s /bin/ash -G weechat -D -u ${UGID} weechat
echo "screen -S weechat -r" > /data/.profile
if [ -n "$SSH_AUTHKEY" ] && ! grep "$SSH_AUTHKEY" /data/.ssh/authorized_keys > /dev/null 2>&1; then
  echo "Adding authorized key/s via SSH_AUTHKEY variable."
  mkdir -p /data/.ssh
  echo "$SSH_AUTHKEY" >> /data/.ssh/authorized_keys
elif [ -f /import/authorized_keys ]; then
  echo "Importing authorized keys file from /import/authorized_keys."
  cp -vf /import/authorized_keys /data/.ssh/authorized_keys
fi
chown -R weechat:weechat /data
chmod 700 /data/.ssh
chmod 600 /data/.ssh/authorized_keys
echo "The following keys are authorized for SSH:"
cat /data/.ssh/authorized_keys

# SCREEN WEECHAT SESSION
if grep "9000" ${WEECHAT_HOME}/relay.conf > /dev/null 2>&1; then
  echo "Starting WeeChat Screen session"
  su-exec weechat screen -dmS weechat weechat \
    || ( echo "Error $? while creating Screen session"; exit 1 )
else
  echo "Initializing new WeeChat Screen session"
  su-exec weechat screen -dmS weechat weechat \
    -r "/mouse enable;/relay add weechat 9000;/set relay.network.password ${WEECHAT_RELAY_PASSWORD}" \
    || ( echo "Error $? while creating Screen session"; exit 1 )
fi

tail -qF "${WEECHAT_HOME}/weechat.log" &

# GLOWING BEAR
mkdir -p /app
cd /app || ( echo "/app not accessible"; exit 1 )
echo "Refreshing Glowing-Bear via Git"
git fetch > /dev/null && echo "Glowing-Bear Git refresh succeeded" || echo "Glowing-Bear Git refresh FAILED"
echo "Running HTTP Server for Glowing-Bear on port 8000"
python -m SimpleHTTPServer 8000 &

# SSHD
echo "Preparing and running sshd"
su-exec weechat mkdir -p /data/etc/ssh
touch /run/sshd.pid
chown weechat:weechat /run/sshd.pid
chmod 644 /run/sshd.pid
su-exec weechat ssh-keygen -A -f /data
chmod 700 /data/etc/ssh
chmod 600 /data/etc/ssh/ssh_host_*_key
chmod 644 /data/etc/ssh/ssh_host_*_key.pub
echo "Port 60000 exposed for MoSH"
su-exec weechat /usr/sbin/sshd -D -e -p 2222 \
  -o "StrictModes no" \
  -o "ClientAliveInterval 300" \
  -o "PasswordAuthentication no" \
  -h /data/etc/ssh/ssh_host_ecdsa_key \
  -h /data/etc/ssh/ssh_host_ed25519_key \
  -h /data/etc/ssh/ssh_host_rsa_key